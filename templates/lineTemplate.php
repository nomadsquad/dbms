<?php
    $line = $_GET['line'];
?>

<?php echo "
<div class=\"row table_row\">
    <div class=\"input-field col s2\">
        <input type=\"text\" class=\"validate name\" placeholder=\"Name\">
    </div>
    <div class=\"input-field col s2\">
        <select class=\"type\">
            <option disabled selected>Type</option>
            <option value=\"INTEGER\">INTEGER</option>
            <option value=\"VARCHAR\">VARCHAR</option>
        </select>
    </div>
    <div class=\"input-field col s2\">
        <input type=\"text\" class=\"validate length\" placeholder=\"Length\">
    </div>
    <div class=\"input-field col s2\">
        <select class=\"default\">
            <option disabled selected>Default</option>
            <option value=\"NULL\">NOT NULL</option>
            <option value=\"NOT NULL\">NULL</option>
        </select>
    </div>
    <div class=\"input-field col s2\">
        <select class=\"extra\">
            <option disabled selected>Extra</option>
            <option value=\"NONE\">None</option>
            <option value=\"PRIMARY KEY\">Primary key</option>
            <option value=\"UNIQUE\">Unique key</option>
        </select>
    </div>
    <div class=\"input-field col s2\" style=\"top: 13px\">
        <span class=\"material-icons remove_line\" data-line=".$line." style=\"cursor: pointer\" onclick=\"deleteRow(".$line.")\">clear</span>
    </div>
</div> "?>