<?php
    include("assets/includes/header.php");
    include('methods/getStructures.php');

?>

<div class="container">

    <div class="row">
        <div class="col s4" id="jstree"><?php listFolderFiles("db") ?></div>

        <div class="col s8" id="showNavigation">

            <input id="db_name" class="form-control" value="<?= substr($_GET["db"], 0, -3) ?>" style="display: none;">
            <input id="table_name" class="form-control" value="<?= $_GET["table"]?>" style="display:none;">

            <div class="col s12">
                <ul class="tabs grey lighten-5">
                    <li class="tab col s3"><a class="active indigo-text text-lighten-3" href="#panel1">Navigation</a></li>
                    <li class="tab col s3"><a class="indigo-text text-lighten-3" href="#panel2">Structure</a></li>
                    <li class="tab col s3"><a class="indigo-text text-lighten-3" href="#panel3">Options</a></li>
                </ul>
            </div>

            <div id="panel1" class="col s12">
                <div id="show_table">
                    <br>
                    <a href='createStructures.php?db=<?=$_GET["db"]?>&table=<?= $_GET["table"]?>' class="waves-effect waves-light btn">Add Data</a>
                    <div class="table-responsive">
                        <table class="highlight">
                            <thead>
                            <tr>
                                <th>Options</th>
                                <?php
                                    foreach ($columns as $column){
                                        echo "<th>".$column['name']."</th>";
                                    }
                                ?>
                            </tr>
                            </thead>
                            <tbody id="navigation">
                            <?php
                                foreach ($table_data as $row){
                                    echo "<tr>";
                                    echo "<td align=\"center\" valign=\"middle\">
                                    <a href='editStructures.php?db=" . $_GET["db"]. "&table=" . $table_name . "&id=" . $row['ID'] . "' style=\"font-size: 14px;\"><i class=\"tiny material-icons\" style=\"display: inline\">mode_edit</i>Edit</a>
                                    <a onclick='deleteData(".$row['ID'].")' style=\"font-size: 14px;  cursor: pointer\"><i class=\"tiny material-icons\" style=\"display: inline\">delete</i>Delete</span>
                                    </td>";

                                    foreach($row as $key => $data_row) {
                                        echo "<td>$data_row</td>";
                                    }
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="panel2" class="col s12">

                <div id="show_table">
                    <br>
                    <div class="table-responsive">
                        <table class="highlight">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Length</th>
                                <th>Default</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

//                            var_dump($table_info);
                                foreach ($columns as $column){

                                    $id = $column["cid"] + 1;
                                    $default = "";
                                    if($column["notnull"] == 0){
                                        $default = "NULL";
                                    } else {
                                        $default = "NOT NULL";
                                    }
                                    $type = explode("(", $column['type'])[0];

                                    echo "<tr>";

                                        echo "<td>". $id ."</td>";
                                        echo "<td>". $column['name'] ."</td>";
                                        echo "<td>". $type ."</td>";


                                        if($column['name'] != "ID") {
                                            if(strpos($column['type'], "(") !== false) {
                                                $length = substr(explode("(", $column['type'])[1], 0, -1);
                                                echo "<td>". $length ."</td>";
                                            } else {
                                                echo "<td></td>";
                                            }

                                        }else{
                                            echo "<td></td>";

                                        }
                                        echo "<td>". $default ."</td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="panel3" class="col s12">
                <h4>Table options</h4>
                <label>Table name</label>
                <input id="edit_table_name" class="form-control" value="<?= $_GET["table"]; ?>">
                <button id="edit_table" class="btn btn-success blue right">Edit table</button>

                    <a id="table-settings" class="btn-floating btn-large teal">
                        <i class="large material-icons">settings</i>
                    </a>

                    <div id="scale-settings" style="
                        position: fixed;
                        transform: translate(8vh, -6vh);"
                        >
                        <br>
                        <div class="chip scale-transition scale-out" style="height: 40px;">
                            <a id="delete-table" data-table="<?= $_GET["table"]; ?>" class="btn-floating red"><i class="material-icons">delete_sweep</i></a><span> Drop table</span>
                        </div>
                        <br>
                        <div class="chip scale-transition scale-out" style="height: 40px;">
                            <a id="empty-table" data-table="<?= $_GET["table"]; ?>" class="btn-floating yellow"><i class="material-icons">redo</i></a><span> Empty table</span>
                        </div>
                    </div>
            </div>
        </div>

    </div>
</div>

<?php
    include("assets/includes/footer.php")
?>