<?php
include("assets/includes/header.php")
?>

    <div class="container">
        <div class="row">
            <div class="col s4" id="jstree"><?php listFolderFiles("db") ?></div>

            <div class="col s8">
                <input id="db_name" value="<?= $_GET["db"] ?>" style="display: none;">
                <h3>Edit database name</h3>

                <label>Database Name</label>
                <input id="edit_db_name" class="form-control" value="<?= substr($_GET["db"], 0, -3) ?>">
                <button id="db_edit" class="btn btn-success right">Edit database</button>
                <button id="db_drop" class="btn red left">Drop database</button>

            </div>
        </div>
    </div>

<?php
include("assets/includes/footer.php")
?>