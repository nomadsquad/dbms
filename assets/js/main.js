$(document).ready(function() {
    var count_scale_settings = 0;

    $('select').material_select();
    $('#jstree').jstree();

    $("#back").click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#table_name").val();
        window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + table_name);

    });

    $('#table-settings').click(function () {

        if(count_scale_settings === 0){
            $('.chip').removeClass("scale-out");
            $('.chip').addClass("scale-in");
            count_scale_settings++;
        } else {
            $('.chip').removeClass("scale-in");
            $('.chip').addClass("scale-out");
            count_scale_settings--;
        }
    });

    $('#new_db').click(function () {
        window.location.replace("create_db.php")
    });

    $('#db_add').click(function () {

        var str = $('#db_name').val();

        if(isValid(str) === false){
            Materialize.toast('Please enter an valid database name!', 4000);
            throw StopIteration;

        }
        if(str === ""){
            Materialize.toast('Database name is empty!', 4000);

            throw StopIteration;
        }

        $.ajax({
            url:'db/'+str+'.db',
            type:'HEAD',
            error: function()
            {
                $.ajax({
                    url:'methods/createDatabase.php',
                    data:{
                        name: str
                    },
                    type: 'POST',
                    success: function()
                    {
                        window.location.replace("create_table.php?db="+str)
                    }
                });
            },
            success: function()
            {
                Materialize.toast('Database already exists!', 4000);
            }
        });
    });

    $("#db_edit").click(function () {
        var db_name = $("#db_name").val();
        var new_db_name = $("#edit_db_name").val();

        if(new_db_name !== "") {
            if (isValid(table_name) === false) {
                Materialize.toast('Syntax Error. Spaces and special characters are not permitted', 4000);

                throw StopIteration;
            }
        } else {
            Materialize.toast('Database name is empty!', 4000);

            throw StopIteration;
        }

        $.ajax({
            url:'methods/editDatabase.php',
            data:{
                status: "edit",
                db_name: db_name,
                new_db_name: new_db_name
            },
            type: 'POST',
            success: function()
            {
                window.location.replace("show_table.php")
            }
        });
    });

    $("#db_drop").click(function () {
        var db_name = $("#db_name").val();

        if (confirm('Are you sure you want to delete this database?')) {
            $.ajax({
                url:'methods/editDatabase.php',
                data:{
                    status: "delete",
                    db_name: db_name
                },
                type: 'POST',
                success: function()
                {
                    window.location.replace("create_db.php")
                }
            });
        }

    });

    $('#add_line').click(function () {
        var line = $(".table_row:last-child").find("span.material-icons").attr("data-line");
        var next_line = parseInt(line) + 1;

        if(line === undefined){
            $.ajax({
                url: "templates/lineTemplate.php",
                data: {
                    line: 1
                },
                type: 'GET',
                success: function (data) {
                    $("#add_new_table").append(data);

                    $('select').material_select();

                }
            });
        } else {
            $.ajax({
                url: "templates/lineTemplate.php",
                data: {
                    line: next_line
                },
                type: 'GET',
                success: function (data) {
                    $("#add_new_table").append(data);

                    $('select').material_select();

                }
            });
        }
    });

    $('#table_add').click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#table_name").val();
        var name_array = [];
        var type_array = [];
        var length_array = [];
        var default_array = [];
        var extra_array = [];

        if(table_name !== "") {
            if (isValid(table_name) === false) {
                Materialize.toast('Syntax Error. Spaces and special characters are not permitted', 4000);

                throw StopIteration;
            }
        } else {
            Materialize.toast('Table name is empty!', 4000);

            throw StopIteration;
        }

        $(".name").each(function (index, value) {
            if($(value).val() !== "") {
                if(isValid($(value).val()) === true){
                    name_array.push($(value).val());

                } else {
                    Materialize.toast('Syntax Error. Spaces and special characters are not permitted', 4000);

                    throw StopIteration;
                }
            } else {
                Materialize.toast('Please fill all the inputs before creating a new table!', 4000);

                throw StopIteration;
            }
        });

        $("select.type").each(function (index, value) {
            if($(value).val() !== "") {
                type_array.push($(value).val());
            } else {
                Materialize.toast('Please fill all the inputs before creating a new table!', 4000);

                throw StopIteration;
            }
        });

        $(".length").each(function (index, value) {
            if($(value).val() !== "") {
                if(isNumeric($(value).val()) === true){
                    length_array.push($(value).val());

                } else {
                    Materialize.toast('Please insert a numeric value on length', 4000);

                    throw StopIteration;
                }
            } else {
                Materialize.toast('Please fill all the inputs before creating a new table!', 4000);

                throw StopIteration;
            }
        });

        $("select.default").each(function (index, value) {
            if($(value).val() !== "") {
                default_array.push($(value).val());
            } else {
                Materialize.toast('Please fill all the inputs before creating a new table!', 4000);

                throw StopIteration;
            }
        });

        $("select.extra").each(function (index, value) {
            if($(value).val() !== "") {
                extra_array.push($(value).val());
            } else {
                Materialize.toast('Please fill all the inputs before creating a new table!', 4000);

                throw StopIteration;
            }
        });

        $.ajax({
            url: "methods/createTable.php",
            data: {
                db_name: db_name,
                table_name: table_name,
                array_name: name_array,
                array_type: type_array,
                array_length: length_array,
                array_default: default_array,
                array_extra: extra_array
            },
            type: 'POST',
            success: function (data) {

                window.location.replace("show_table.php?db="+db_name+"&table="+table_name);

            }
        });
    });

    $("#edit_table").click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#table_name").val();
        var new_table_name = $("#edit_table_name").val();

        if(new_table_name === "") {
            Materialize.toast('Please enter the table name', 4000);

            return;
        }

        if(isValid(new_table_name) !== true){
            Materialize.toast('Syntax Error. Spaces and special characters are not permitted', 4000);

            return;
        }

        $.ajax({
            url: "methods/editTable.php",
            data: {
                db_name: db_name,
                table_name: table_name,
                new_table_name: new_table_name,
                status: "edit"
            },
            type: 'POST',
            success: function (data) {
                window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + data);

            }
        });
    });

    $("#delete-table").click(function () {

        if(confirm("Are you sure you want to delete this table?")) {
            var db_name = $("#db_name").val();
            var table_name = $("#delete-table").attr("data-table");

            $.ajax({
                url: "methods/editTable.php",
                data: {
                    db_name: db_name,
                    table_name: table_name,
                    status: "delete"
                },
                type: 'POST',
                success: function (data) {
                    window.location.replace("show_db.php?db=" + db_name + ".db");
                }
            });
        }
    });

    $("#empty-table").click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#empty-table").attr("data-table");

        if(confirm("Are you sure you want to empty this table?")) {
            $.ajax({
                url: "methods/editTable.php",
                data: {
                    db_name: db_name,
                    table_name: table_name,
                    status: "empty"
                },
                type: 'POST',
                success: function (data) {
                    window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + table_name);
                }
            });
        }
    });

    $("#add_data").click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#table_name").val();
        var data_array = [];
        var type = "";
        var name = "";
        var length = "";
        var default_data = "";
        var data_value = "";

            $(".data").each(function (index, value) {
                var data_object = {};

                data_value = $(value).val();
                name = $(value).attr("data-name");
                default_data = $(value).attr("data-default");

                if ($(value).attr("data-type").indexOf('(') > -1) {
                    type = $(value).attr("data-type").split("(")[0];
                    length = $(value).attr("data-type").split("(")[1].slice(0, -1);
                } else {
                    type = $(value).attr("data-type");
                    length = 20000;
                }

                if(default_data === "NOT NULL" && data_value === "") {
                    Materialize.toast('Please fill all the inputs in order to proceed', 4000);

                    throw StopIteration;
                }


                if(type === "INTEGER"){
                    if(isNumeric(data_value) === true){
                        if(data_value.length <= length){
                            data_object.value = $(value).val();
                            data_object.name = $(value).attr("data-name");
                            data_object.type = $(value).attr("data-type");
                            data_array.push(data_object);
                        } else {
                            Materialize.toast(name + " has a limit of " + length, 4000);
                        }
                    } else {
                        Materialize.toast(name + " must be integer", 4000);

                        throw StopIteration;
                    }
                }

                if(type === "VARCHAR"){
                    if(isValid(data_value) === true){
                        if(data_value.length <= length){
                            data_object.value = $(value).val();
                            data_object.name = $(value).attr("data-name");
                            data_object.type = $(value).attr("data-type");
                            data_array.push(data_object);
                        } else {
                            Materialize.toast(name + " has a limit of " + length, 4000);
                        }
                    } else {
                        Materialize.toast(name + " doesn't accept special characters", 4000);

                        throw StopIteration;
                    }
                }
            });

            $.ajax({
                url: "methods/createStructures.php",
                data: {
                    db_name: db_name,
                    table_name: table_name,
                    data_array: data_array
                },
                type: 'POST',
                success: function (data) {
                    window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + table_name);
                }
            });
    });


    $("#edit_data").click(function () {
        var db_name = $("#db_name").val();
        var table_name = $("#table_name").val();
        var id = $("#id_column").val();
        var data_array = [];
        var type = "";
        var name = "";
        var length = "";
        var default_data = "";
        var data_value = "";

        $(".data").each(function (index, value) {
            var data_object = {};

            data_value = $(value).val();
            name = $(value).attr("data-name");
            default_data = $(value).attr("data-default");

            if ($(value).attr("data-type").indexOf('(') > -1) {
                type = $(value).attr("data-type").split("(")[0];
                length = $(value).attr("data-type").split("(")[1].slice(0, -1);
            } else {
                type = $(value).attr("data-type");
                length = 20000;
            }

            if(default_data === "NOT NULL" && data_value === "") {
                Materialize.toast('Please fill all the inputs in order to proceed', 4000);

                throw StopIteration;
            }


            if(type === "INTEGER"){
                if(isNumeric(data_value) === true){
                    if(data_value.length <= length){
                        data_object.value = $(value).val();
                        data_object.name = $(value).attr("data-name");
                        data_object.type = $(value).attr("data-type");
                        data_array.push(data_object);
                    } else {
                        Materialize.toast(name + " has a limit of " + length, 4000);
                    }
                } else {
                    Materialize.toast(name + " must be integer", 4000);

                    throw StopIteration;
                }
            }

            if(type === "VARCHAR"){
                if(isValid(data_value) === true){
                    if(data_value.length <= length){
                        data_object.value = $(value).val();
                        data_object.name = $(value).attr("data-name");
                        data_object.type = $(value).attr("data-type");
                        data_array.push(data_object);
                    } else {
                        Materialize.toast(name + " has a limit of " + length, 4000);
                    }
                } else {
                    Materialize.toast(name + " doesn't accept special characters", 4000);

                    throw StopIteration;
                }
            }
        });

        $.ajax({
            url: "methods/editStructures.php",
            data: {
                db_name: db_name,
                table_name: table_name,
                id: id,
                data_array: data_array,
                status: "edit"
            },
            type: 'POST',
            success: function (data) {

                window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + table_name);
            }
        });
    });



});

function isValid(str) { return /^\w+$/.test(str); }
function isNumeric(val) { return /^\d+$/.test(val); }

function deleteRow(data){
    $(".material-icons[data-line='"+ data +"']").parent().parent().remove();
}

function deleteData(id){
    var db_name = $("#db_name").val();
    var table_name = $("#table_name").val();

    if(confirm("Are you sure you want to delete this data?")) {
        $.ajax({
            url: "methods/editStructures.php",
            data: {
                db_name: db_name,
                table_name: table_name,
                id: id,
                status: "delete"
            },
            type: 'POST',
            success: function (data) {
                window.location.replace("show_table.php?db=" + db_name + ".db" + "&table=" + table_name);
            }
        })
    }
}


