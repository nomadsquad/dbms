<?php

include('connectDatabase.php');

function listFolderFiles($dir){
    $ffs = scandir($dir);

    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1)
        return;

    echo '<ul>';
    echo '<li data-jstree=\'{"icon": "add-image"}\' id="new_db">Add new database</li>';
    foreach($ffs as $ff){
        echo '<li data-jstree=\'{"icon": "add-image-db"}\'><span onClick="javascript:window.location.href=\'show_db.php?db='.$ff.'\'">'.substr($ff, 0, -3)."</span>";

        $tables = getTables(substr($ff, 0, -3));

        if(gettype($tables) == "array" && !empty($tables)){
            echo '<ul>';

            echo '<li data-jstree=\'{"icon":"add-image"}\' class="new_table"><span onClick="javascript:window.location.href=\'create_table.php?db='.$ff.'\'" >Add new table</span></li>';
            if(count($tables) > 0){
                foreach($tables as $data) {
                    echo '<li data-jstree=\'{"icon": "add-image-table"}\'><span onClick="javascript:window.location.href=\'show_table.php?db='.$ff.'&table='.$data.'\'">' . $data . '</span></li>';
                }
            } else {
                echo '<li data-jstree=\'{"icon": "add-image-table"}\'><span onClick="javascript:window.location.href=\'show_table.php?db='.$ff.'&table='.$data.'\'">' . $tables[0] . '</span></li>';
            }

            echo '</ul>';
        } else {
            echo '<ul>';
            echo '<li data-jstree=\'{"icon":"add-image"}\' class="new_table"><span onClick="javascript:window.location.href=\'create_table.php?db='.$ff.'\'" >Add new table</span></li>';
            echo '</ul>';

        }

        echo '</li>';
    }
    echo '</ul>';
    echo '<span id="tree_load"></span>';
}